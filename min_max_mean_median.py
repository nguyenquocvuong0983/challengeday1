# Enter a list of numbers. Find:
# min, max
# mean
# median
import sys
def min_max(numbers):
    min_number = numbers[0]
    max_number = numbers[0]    
    
    for number in numbers[1:]:
        if number < min_number:
            min_number = number
        else:
            pass
        if number > max_number:
            max_number = number
    return min_number, max_number


def avg(numbers):
    total = 0
    
    for number in numbers:
        total += number
    return total/len(numbers)


def median_(numbers):
    if len(numbers) % 2 == 0:
        mid_number_1 = (len(numbers) - 1) // 2
        min_number_2 = mid_number_1 + 1
        return (numbers[mid_number_1] + numbers[min_number_2]) / 2
    else:
        return numbers[(len(numbers)-1) // 2]


if __name__=="__main__":
    # Enter a string of numbers, separate by a space. Ex: '78 78 95 12 -98 54' 
    nums = sys.argv[1].split()
    numbers = list(map(float, nums))
    min_value, max_value = min_max(numbers)
    mean = avg(numbers)
    median = median_(numbers)
    print("min:", min_value)
    print("max:", max_value)
    print("mean:", mean)
    print("median:", median)