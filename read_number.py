# Đọc chuỗi ký số cho trước bằng tiếng Việt
# Chương trình em viết bao gồm:
#   1 dictionary chứa các test_cases.
#   1 hàm tách hàng thập phân, mỗi hàng thập phâm gồm 3 ký số - split_decimal().
#   1 hàm đọc số các hàng thập phân - read_number().
#   1 hàm kiểm tra giá trị hàng ngàn, triệu, tỉ để hiển thị chữ 'tỉ', 'triệu', 'ngàn' - check_decimal().
#   Các dictionaries num_dict1, 2, 3, 4 để bỏ bớt các điều kiện if-else.
test_cases = {
    'test1': {'input': '0', 'output': 'Không.'},
    'test2': {'input': '00', 'output': 'Không.'},
    'test3': {'input': '0001', 'output': 'Một.'},
    'test4': {'input': '10', 'output': 'Mười.'},
    'test5': {'input': '11', 'output': 'Mười một.'},
    'test6': {'input': '20', 'output': 'Hai mươi.'},
    'test7': {'input': '21', 'output': 'Hai mươi mốt.'},
    'test8': {'input': '15', 'output': 'Mười lăm.'},
    'test9': {'input': '95', 'output': 'Chín mươi lăm.'},
    'test10': {'input': '99', 'output': 'Chín mươi chín.'},
    'test11': {'input': '100', 'output': 'Một trăm.'},
    'test12': {'input': '101', 'output': 'Một trăm lẻ một.'},
    'test13': {'input': '110', 'output': 'Một trăm mười.'},
    'test14': {'input': '121', 'output': 'Một trăm hai mươi mốt.'},
    'test15': {'input': '155', 'output': 'Một trăm năm mươi lăm.'},
    'test16': {'input': '301', 'output': 'Ba trăm lẻ một.'},
    'test17': {'input': '654', 'output': 'Sáu trăm năm mươi bốn.'},
    'test18': {'input': '987', 'output': 'Chín trăm tám mươi bảy.'},
    'test19': {'input': '000987', 'output': 'Chín trăm tám mươi bảy.'},
    'test21': {'input': '1000', 'output': 'Một ngàn.'},
    'test22': {'input': '13056', 'output': 'Mười ba ngàn không trăm năm mươi sáu.'},
    'test23': {'input': '106532', 'output': 'Một trăm lẻ sáu ngàn năm trăm ba mươi hai.'},
    'test24': {'input': '1000000', 'output': 'Một triệu.'},
    'test25': {'input': '1000011', 'output': 'Một triệu không trăm mười một.'},
    'test26': {'input': '10000000', 'output': 'Mười triệu.'},
    'test27': {'input': '32165169', 'output': 'Ba mươi hai triệu một trăm sáu mươi lăm ngàn một trăm sáu mươi chín.'},
    'test28': {'input': '031304618', 'output': 'Ba mươi mốt triệu ba trăm lẻ bốn ngàn sáu trăm mười tám.'},
    'test29': {'input': '3402164210', 'output': 'Ba tỉ bốn trăm lẻ hai triệu một trăm sáu mươi bốn ngàn hai trăm mười.'},
    'test30': {'input': '1023642020', 'output': 'Một tỉ không trăm hai mươi ba triệu sáu trăm bốn mươi hai ngàn không trăm hai mươi.'},
    'test31': {'input': '984116021654', 'output': 'Chín trăm tám mươi bốn tỉ một trăm mười sáu triệu không trăm hai mươi mốt ngàn sáu trăm năm mươi bốn.'},
    'test32': {'input': '000043116726', 'output': 'Bốn mươi ba triệu một trăm mười sáu ngàn bảy trăm hai mươi sáu.'},
    'test33': {'input': '970000715717', 'output': 'Chín trăm bảy mươi tỉ bảy trăm mười lăm ngàn bảy trăm mười bảy.'},
    'test34': {'input': '246040427370', 'output': 'Hai trăm bốn mươi sáu tỉ không trăm bốn mươi triệu bốn trăm hai mươi bảy ngàn ba trăm bảy mươi.'},
    'test35': {'input': '1000200031', 'output': 'Một tỉ hai trăm ngàn không trăm ba mươi mốt.'},
    'test36': {'input': '8422159810', 'output': 'Tám tỉ bốn trăm hai mươi hai triệu một trăm năm mươi chín ngàn tám trăm mười.'},
    'test37': {'input': '3216321002', 'output': 'Ba tỉ hai trăm mười sáu triệu ba trăm hai mươi mốt ngàn không trăm lẻ hai.'},
    'test38': {'input': '68984613200', 'output': 'Sáu mươi tám tỉ chín trăm tám mươi bốn triệu sáu trăm mười ba ngàn hai trăm.'},
    'test39': {'input': '3271470023', 'output': 'Ba tỉ hai trăm bảy mươi mốt triệu bốn trăm bảy mươi ngàn không trăm hai mươi ba.'},
    'test40': {'input': '1000000001', 'output': 'Một tỉ không trăm lẻ một.'}
}

# Hàm tách số phần thập phân: hàng tỉ - hàng triệu - hàng ngàn - hàng trăm.
# Đầu tiên gán tất cả các phần thập phân là một chuỗi không có ký tự.
# Thực hiện vòng lặp từ cuối đến đầy chuỗi số theo thức tự là hàng đơn vị đến hàng tỉ.
# Sau mỗi lần lặp, nối các ký số vào chuỗi theo thứ tự thập phân tương ứng.
# Nếu đủ 3 ký số thì các ký số tiếp gắn cho hàng thập phân cao hơn. Lặp lại cho đến khi hết chuỗi số

# Tiếp theo kiểm tra các hàng thập phân có nghĩa hay không. Bằng cách loại bỏ các ký số '0' vô nghĩa bên trái chuỗi số.
# Nếu hàng thập phân phía trước nó là một chuỗi rỗng thì các số không bên trái hàng thập phân hiện tại là vô nghĩa.
def split_decimal(nums):
    i = -1
    hang_tram = ""
    hang_ngan = ""
    hang_trieu = ""
    hang_ti = ""
    while i >= -len(nums):
        if i > -4:
            hang_tram = nums[i] + hang_tram
        elif i > -7:
            hang_ngan = nums[i] + hang_ngan
        elif i > -10:
            hang_trieu = nums[i] + hang_trieu
        else:
            hang_ti = nums[i] + hang_ti
        i -= 1

    if hang_ti.lstrip('0') == "":                       # Xử lý tối đa chuỗi 12 ký số --> Hàng tỉ không có chuỗi trước nó
        hang_trieu = hang_trieu.lstrip('0')             # -- > Loại ký số '0' không cần điều kiện.
        if hang_trieu == "":
            hang_ngan = hang_ngan.lstrip('0')           
            if hang_ngan == "":
                hang_tram = hang_tram.lstrip('0')  
    return hang_ti, hang_trieu, hang_ngan, hang_tram


num_dict1 = {"1": "một", "2": "hai", "3": "ba", "4": "bốn", "5": "năm", "6": "sáu", "7": \
    "bảy", "8": "tám", "9": "chín", "0": "", "":""}
num_dict2 = {"1": "một", "2": "hai", "3": "ba", "4": "bốn", "5": "lăm", "6": "sáu", "7": \
    "bảy", "8": "tám", "9": "chín", "0": "", "":""}
num_dict3 = {"1": "mốt", "2": "hai", "3": "ba", "4": "bốn", "5": "lăm", "6": "sáu", "7": \
    "bảy", "8": "tám", "9": "chín", "0": "", "":""}
num_dict4 = {"0": 0,"1": 1, "2": 1,"3": 1,"4": 1,"5": 1,"6": 1,"7": 1,"8": 1,"9": 1, "": 0}

# Hàm read_number kết hợp các điều kiện và dictionary để đọc chuỗi ký số của mỗi hàng thập phân.
# Vì mỗi hàng thập phân chỉ khác nhau các chữ 'tỉ', 'triệu', 'ngàn' đứng phía sau nó nên việc xử lý các ký số của mỗi hàng là giống nhau.
# num_dict1: đọc các ký số hàng trăm, hàng chục.
# num_dict2, num_dict3: đọc các ký số hàng đơn vị với hàng chục tương ứng. Ví dụ: lẻ năm, mười một, hai mươi mốt, mười lăm, năm mươi lăm.
# num_dict4: dùng làm điều kiện để đọc các số như: một ngàn không trăm lẻ một mà không bị lặp chữ 'trăm'.
def read_number(nums):
    if nums == "":          # Trường hợp chuỗi được cho chỉ có 3 ký số bằng '0', '00' hoặc '000'.
        return "không"      # Khi loại ký số '0' thì nums là một chuỗi rỗng. Quy ước đọc là 'không'.
                            # Có thể đặt điều kiện chiều dài chuỗi được cho nhỏ hơn 4 để không lọc ký số '0'.

    don_vi = nums[-1]       # chia 3 ký số của các hàng thập phân lại thành hàng trăm, chục và đơn vị.
    chuc = ""               # tùy theo độ dài của chuỗi số mà hàng chục có vị trí khác nhau.
    tram = ""               # hàng trăm luôn ở vị trí [0], hàng đơn vị luôn ở vị trí [-1].

    if len(nums) == 2:
        chuc = nums[0]
    elif len(nums) == 3:
        chuc = nums[1]
        tram = nums[0]

# Đọc số
# Dựa vào độ dài chuỗi nums và giá trị của hàng trăm, hàng chục, kết hợp với các num_dict để đọc.
# Trường hợp độ dài chuỗi bằng 1: Số đã được loại bỏ ký số '0' vô nghĩa. Đọc là một, hai,.. đến chín tương ứng num_dict1.
# Trường hợp độ dài chuỗi bằng 2 hoặc 3:
#   -  Nếu hàng chục, trăm bằng "", hoặc một ký số khác '0'. Đọc số với hàng chục tương ứng mà không kết hợp với 'không trăm'. Ví dụ: Không, năm, mười một, hai mốt, hai lăm, một trăm lẻ năm...
#   - Nếu hàng chục, hàng trăm là ký số '0'. Đọc số kết hợp với 'không trăm'. Ví dụ: một ngàn không trăm lẻ một, mười lăm triệu không trăm hai mốt ngàn không trăm hai mươi mốt,...
    if len(nums) == 1:
        return num_dict1[don_vi]
    elif len(nums) >= 2:
        if chuc == '0':
            if don_vi == '0':
                return num_dict1[tram] + num_dict4[tram]*" trăm " + " "
            else:
                if tram == '0':
                    return "không trăm" + " lẻ " + num_dict1[don_vi] + " "
                else:
                    return num_dict1[tram] + num_dict4[tram]*" trăm" + " lẻ " + num_dict1[don_vi] + " "
        elif chuc == '1':
            if tram == '0':
                return 'không trăm' + " mười " + num_dict2[don_vi] + " "
            else:
                return num_dict1[tram] + num_dict4[tram]*' trăm' + " mười " + num_dict2[don_vi] + " "
        else:
            if tram == '0':
                return 'không trăm ' + num_dict1[chuc] + " mươi " + num_dict3[don_vi]
            else:
                return num_dict1[tram] + num_dict4[tram]*' trăm ' + num_dict1[chuc] + " mươi " + num_dict3[don_vi]


# Hàm check_decimal trả về các điều kiện để đọc các phần thập phân hàng tỉ, triệu, ngàn.
# Nếu không các hàng thập phân tương ứng. Dùng giá trị check nhân với chuỗi 'tỉ', 'triệu', 'ngàn' sẽ trả về chuỗi rỗng "".
# Không gộp với hàm split_decimal() để loại các trường hợp không ngàn, không triệu, không tỉ và đỡ rối.
def check_decimal(ti, trieu, ngan, tram):
    check_hang_ngan = 0
    check_hang_trieu = 0
    check_hang_ti = 0
    if hang_ngan.lstrip('0') != "":    
        check_hang_ngan = 1
    if hang_trieu.lstrip('0') != "":
        check_hang_trieu = 1
    if hang_ti.lstrip('0') != "":
        check_hang_ti = 1        
    return check_hang_ngan, check_hang_trieu, check_hang_ti


if __name__ == "__main__":
    test_failed = 0
    for test in test_cases:
        hang_ti, hang_trieu, hang_ngan, hang_tram = split_decimal(test_cases[test]['input'])
        check_hang_ngan, check_hang_trieu, check_hang_ti = check_decimal(hang_ti, hang_trieu, hang_ngan, hang_tram)

        day_so = check_hang_ti*read_number(hang_ti) + check_hang_ti*" tỉ " + check_hang_trieu*read_number(hang_trieu)\
            + check_hang_trieu*" triệu "+ check_hang_ngan*read_number(hang_ngan) + check_hang_ngan*" ngàn "  +\
                read_number(hang_tram)

        day_so = " ".join(day_so.split())               # lọc các khoảng trắng thừa.
        day_so = day_so.capitalize() + "."              # in hoa ký tự đầu và thêm dấu chấm ở cuối.
        
        if day_so != test_cases[test]['output']:        # nếu test_case bị sai thì đếm số lượng và in ra các cách đọc sai.
            test_failed += 1
            print(f"Testcase -- '{test}' -- failed!")
            print(day_so + ' <---> ' + test_cases[test]['output'])

    print("Test failed:", test_failed)


# OUTPUT: Với các cách đọc được quy ước trước. Chỉ đọc các phần thập phân hợp lệ cộng với hàng trăm.
# Ví dụ:
#       1021: một ngàn không trăm hai mốt
#       1001: một ngàn không trăm lẻ một.
#       1000015: một triệu không trăm mười lăm.
#       1000215000: một tỉ hai trăm mười lăm ngàn.

# Các test_cases hiện tại: Test failed: 0