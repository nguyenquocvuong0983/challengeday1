# Giving 2 numbers have string type.
# Calculating the sum of 2 numbers no using int() function.
import sys

class MyBigNumber():
    def __init__(self, s1, s2):
        self.s1 = s1
        self.s2 = s2

    def sum2(self):
        """calculate the sum of s1 and s2"""
        # get the length of the shorter string.
        if len(self.s1) > len(self.s2):
            lens = len(self.s1)
        else:
            lens = len(self.s2) 

        # 1. loop from the end to the begin of 2 string (-1 is the end value, -lens is the first value, step -1).
        # 2. sum 2 numbers of each index by ASCII.
        # 3. if the sum is greater than 10, save the greater, remember 1.
        # 4. Repeat till no more character.

        nums_str = ""       # string to concatenate.
        total = 0           # sum 2 value at each index and remember.
        remember = 0
        for i in range(-1, -lens-1, -1):
            try:                                    # get value at index i of string s1.
                index1 = f"{self.s1[i]}"            # if yes, get value.
            except IndexError:                      # if no, value is NULL => ASCII "\x00".
                index1 = "\x00"
            try:                                    # similar to string s2.
                index2 = f"{self.s2[i]}"
            except IndexError:
                index2 = "\x00"
                
            if index1 == "\x00" or index2 =="\x00":
                total = remember + ord(index1) + ord(index2) - 48       # calculate the total if having 1 null value.
            else:
                total = remember + ord(index1) -48 + ord(index2) - 48   # calculate the total.
            
            remember = total//10        # if total greater than 10, remember = 1, else 0.

            # concatnate 2 string to get the suitable value. Ex: '2' + '1' = '21', '21' + '2' = '212'.
            nums_str = str(total%10) + nums_str      
        
        if remember == 0:
            return nums_str
        elif remember == 1:
            return "1" + nums_str
        

if __name__=="__main__":
    my_big_number = MyBigNumber(sys.argv[1], sys.argv[2])   # giving 2 argument from terminal command to class.
    final_numbers = my_big_number.sum2()                    
    print(final_numbers.lstrip("0"))                        # remove '0' on the left side of tring.