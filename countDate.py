# Tính tổng số ngày từ 2 mốc thời gian được cho.
# Nếu chỉ cho 1 mốc thì tính tổng số ngày đến thời điểm hiện tại
# Quy ước:
#   1. Nhập ngày theo định dạng : 'yyyy-mm-dd'
#   2. Cách gọi:
#       - Ngày thứ nhất (hoặc ngày trước): Là ngày nhỏ hơn. (2021-01-01)
#       - Ngày thứ hai (hoặc ngày sau): Là ngày lớn hơn. (2021-01-02)
# Chạy file bằng cách nhập lệnh vào terminal:
#       python countDate.py 'day1' 'day2'
# Hoặc  python countDate.py 'day1'
# Không nhất thiết day1 phải nhỏ hơn day2.

from datetime import date
import sys

# Số ngày trong tháng của một năm không phải năm nhuận.
day_of_month = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31}


# Hàm kiểm tra có phải năm nhuận hay không.
def is_leap_year(year):
    if year % 400 == 0 or (year % 4 == 0 and year % 100 != 0):
        return True
    else:
        return False


# Hàm đếm số ngày từ ngày được cho đến cuối năm đó.
def so_ngay_tinh_den_cuoi_nam(day, month, year):
    if is_leap_year(year):          # Nếu là năm nhuận thì tháng 2 có 29 ngày.
        day_of_month[2] = 29

    so_ngay_con_lai_trong_thang = day_of_month[month] - day         # Tổng số ngày từ ngày đó đến cuối năm bằng số ngày còn lại của tháng
    tong_ngay_den_cuoi_nam1 = so_ngay_con_lai_trong_thang           # cộng với tổng số ngày trong các tháng còn lại.

    for i in range(month+1, 13):                                    # Tính từ sau tháng đó, cứ mỗi tháng tăng lên thì cộng số ngày tương ứng
        tong_ngay_den_cuoi_nam1 += day_of_month[i]                  # của tháng. Ví dụ, ngày được cho nằm trong tháng 10
                                                                    # thì cộng 30 ngày của tháng 11, cộng thêm 31 ngày của tháng 12.
    print("Tổng số ngày tính đến cuối năm của ngày thứ nhất (ngày trước): ",tong_ngay_den_cuoi_nam1)
    return tong_ngay_den_cuoi_nam1


# Hàm đếm số ngày từ đầu năm đến ngày được cho của năm đó.
def so_ngay_tu_dau_nam(day, month, year):                           
    if is_leap_year(year):                                          # Lại kiểm tra năm này có phải năm nhuận không riêng biệt.
        day_of_month[2] = 29

    so_ngay_da_di_qua_trong_thang = day                             # Tổng số ngày đã đi qua bằng số ngày của tháng được cho
    tong_ngay_tinh_tu_dau_nam2 = so_ngay_da_di_qua_trong_thang      # cộng với tổng số ngày của các tháng trước đó.

    for i in range(month-1, 0, -1):
        tong_ngay_tinh_tu_dau_nam2 += day_of_month[i]
    print("Tổng số ngày tính từ đầu năm của ngày thứ hai (ngày sau):", tong_ngay_tinh_tu_dau_nam2)
    return tong_ngay_tinh_tu_dau_nam2

# Hàm tách ngày và tính toán.
# Tách ngày, tháng, năm của 2 ngày được cho.
def split_and_calcualate_day(day1, day2):
    day_1 = int(day1[8:])
    month_1 = int(day1[5:7])
    year_1 = int(day1[:4])

    day_2 = int(day2[8:])
    month_2 = int(day2[5:7])
    year_2 = int(day2[:4])

    if day1 > day2:                                     # Quy ước ngày được cho sau lớn hơn ngày trước để dễ tính toán.
        (day_1, day_2) = (day_2, day_1)                 # Nếu ngày thứ nhất lớn hơn ngày thứ hai thì đảo thứ tự lại.
        (month_1, month_2) = (month_2, month_1)         # day2 = day1 và day1 = day2
        (year_1, year_2) = (year_2, year_1)

    print(day_1, month_1, year_1)
    print(day_2, month_2, year_2)

# Tính tổng số ngày giữa 2 mốc thời gian được cho bằng cách:
# Đối với 2 mốc thời gian trong cùng 1 năm thì:
#   - Lấy tổng số ngày tính từ đầu năm của ngày thứ 2 (tức ngày sau) trừ tổng số ngày tính từ đầu năm của ngày trước.
#   - Trường hợp này đã kiểm tra năm nhuận trong khi tính số ngày.
# Đối với 2 mốc thời gian khác năm:
#   - Tính số ngày từ ngày thứ nhất đến cuối năm, cộng với tổng số ngày của số năm chênh lệch, cộng với tổng số ngày đã qua tính từ đầu năm
#     của ngày thứ hai.
#   - Số ngày của năm được cộng phụ thuộc năm đó có là năm nhuận hay không.

    if year_1 == year_2:
        tong_ngay_tu_cuoi_nam1_den_dau_nam2 = so_ngay_tu_dau_nam(day_2, month_2, year_2) - so_ngay_tu_dau_nam(day_1, month_1, year_1)
        return tong_ngay_tu_cuoi_nam1_den_dau_nam2
    else:
        tong_ngay_tu_cuoi_nam1_den_dau_nam2 = so_ngay_tinh_den_cuoi_nam(day_1, month_1, year_1) + so_ngay_tu_dau_nam(day_2, month_2, year_2)
        for year in range(year_1+1, year_2):
            if is_leap_year(year):
                tong_ngay_tu_cuoi_nam1_den_dau_nam2 += 366
            else:
                tong_ngay_tu_cuoi_nam1_den_dau_nam2 += 365
    
    return tong_ngay_tu_cuoi_nam1_den_dau_nam2    

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Missing day...!")
    elif len(sys.argv) == 2:
        day1 = sys.argv[1]
        day2 = date.today().isoformat()
        print(day1, day2)
        total_day = split_and_calcualate_day(day1, day2)
        print("Hai mốc thời gian cách nhau: " + str(total_day) + " ngày.")
    elif len(sys.argv) == 3:
        day1 = sys.argv[1]
        day2 = sys.argv[2]
        print(day1, day2)
        total_day = split_and_calcualate_day(day1, day2)
        print("Hai mốc thời gian cách nhau: " + str(total_day) + " ngày.")
    else:
        print("Only 2 days are needed!")

    

# Bài này em chưa viết test case.
# Reference: https://daynhauhoc.com/t/thao-luan-c-tinh-khoang-thoi-gian-giua-2-thoi-diem-ngay-thang-nam/23812