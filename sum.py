# Giving 2 numbers have string type.
# Calculating the sum of 2 numbers no using int() function.
import sys

class MyBigNumber():
    def __init__(self, s1, s2):
        self.s1 = s1
        self.s2 = s2

    def sum2(self):
        """calculate the sum of s1 and s2"""
        # get the length of the shorter string.
        if len(self.s1) > len(self.s2):
            lens = len(self.s1)
        else:
            lens = len(self.s2) 

# 1. loop from the end to the begin of 2 string (-1 is the end value, -lens is the first value, step -1).
# 2. sum 2 numbers of each index by ASCII with the before remember variable.
# 3. if the sum is greater than 10, save the greater, that is the current remember variable 1.
# 4. Repeat till no more character.

        nums_str = ""           # string to concatenate.
        total = 0               # sum 2 value at each index and remember.
        remember_before = 0      
        for i in range(-1, -lens-1, -1):
            try:                                    # get value at index i of string s1.
                index1 = f"{self.s1[i]}"            # if yes, get value.
            except IndexError:                      # if no, value is NULL => ASCII "\x00".
                index1 = "\x00"
            try:                                    # similar to string s2.
                index2 = f"{self.s2[i]}"
            except IndexError:
                index2 = "\x00"

            if index1 == "\x00" or index2 =="\x00":
                total = remember_before + ord(index1) + ord(index2) - 48       # calculate the total if having 1 null value.
            else:
                total = remember_before + ord(index1) -48 + ord(index2) - 48   # calculate the total.
            
            remember_after = total // 10    # current remember variable if total >= 10, remember_after = 1, else 0
            if remember_after == 1:
                if remember_before == 1:
                    print(f"Lấy '{index1}' cộng '{index2}' => Kết quả được {total-1}, cộng với 1 được {total}, lưu {total%10}, nhớ 1.")
                else:
                    print(f"Lấy '{index1}' cộng '{index2}' => Kết quả được {total}, lưu {total%10}, nhớ 1.")
            else:
                if remember_before == 1:
                    print(f"Lấy '{index1}' cộng '{index2}' => Kết quả được {total-1}, cộng với 1 được {total}, lưu {total%10}.")
                else:
                    print(f"Lấy '{index1}' cộng '{index2}' => Kết quả được {total}, lưu {total}.")

            remember_before = remember_after         

            # concatnate 2 string to get the suitable value. Ex: '2' + '1' = '21', '21' + '2' = '212'.
            nums_str = str(total%10) + nums_str      
        
        if remember_before == 0:
            return nums_str
        elif remember_before == 1:
            return "1" + nums_str
        
# 1. Get argument from command line -- sum s1 s2.
# 2. Check if get enough 2 argument. Repeat this check.
# 3. If it's not enough. Ask to enter again s1, s2 separate by a space -- s1 s2.
# 4. If it's enough. Check if arguments have right type.
# 5. If not, raise an Error and finish.
# 6. If yes, do the sum of 2 argument.

if __name__=="__main__":
    while True:
        if len(sys.argv) < 3:
            print("Missing parameter s1 or s2. You should write 'sum s1 s2'. Enter s1 and s2 again saparately by a space.")
            s1_s2= input().split()
            if len(s1_s2) == 2:
                s1_s2.insert(0, sys.argv[0])
                sys.argv = s1_s2
        else:
            break
    
    s1 = sys.argv[1]
    s2 = sys.argv[2]
    if s1.lstrip('-').isdigit() and s2.lstrip('-').isdigit(): # strip '-' before a negative number, then check
        if int(s1) < 0 or int(s2) < 0:
            raise ValueError("Only positive intergers are allowed!")
        else:
            pass
    else:
        raise TypeError("s1, s2 must be string of numbers!")

    print("Các bước thực hiện:")
    my_big_number = MyBigNumber(s1, s2)     # giving 2 argument from terminal command to class.
    final_numbers = my_big_number.sum2()
    print("Kết quả cuối cùng:", final_numbers.lstrip("0"))      # remove '0' on the left side of tring.



# Reference:
# 1. Package python file to exe -- https://pyinstaller.readthedocs.io/en/stable/usage.html
# 2. Python try - except: https://www.w3schools.com/python/python_try_except.asp
# 3. Calculate digit by ASCII : cách giải của Hậu và hướng dẫn của anh Thạch.
# 4. ASCII table: https://www.cs.cmu.edu/~pattis/15-1XX/common/handouts/ascii.html